package com.example.demo.Model;

    public class Dog extends Mammal{
    

    private String name ;

   
    public Dog(){
        super();
    }

    public Dog(String name) {
        super(name);
    }

    public void greets(){
        System.out.println("woof");
    }

    public void greets(Dog anotherDog){
        System.out.println("woooooooooof");
    }

    @Override
    public String toString() {
        return "Dog [Mammal [Animal [name=" + name + "]";
    }

    
}